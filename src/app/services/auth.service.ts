/* tslint:disable:no-redundant-jsdoc */
import { Injectable } from '@angular/core';
import firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  /**
   * Create new user on firebase
   * @param email
   * @param password
   * @return Promise<any>
   */
  createNewUser(email: string, password: string): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(email, password).then(
          () => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  /**
   * Sign new user
   * @param email
   * @param password
   * @return Promise<any>
   */
  signInUser(email: string, password: string): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(email, password).then(
          () => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  signOutUser(): any {
    firebase.auth().signOut();
  }
}
